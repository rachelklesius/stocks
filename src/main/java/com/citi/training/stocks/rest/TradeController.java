package com.citi.training.stocks.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.stocks.model.Trade;
import com.citi.training.stocks.service.TradeService;

@RestController
@RequestMapping(TradeController.BASE_PATH)
public class TradeController {
	
	public static final String BASE_PATH ="/trades";
	
	@Autowired
	TradeService tradeService;
	
	@RequestMapping(method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Trade> findAll() {
		return tradeService.findAll();
	}
	
	@RequestMapping(method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Trade> create(Trade trade) {
		trade.setId(tradeService.create(trade));
		return new ResponseEntity<Trade>(trade, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE, 
            consumes=MediaType.APPLICATION_JSON_VALUE)
	public Trade findById (int id) {
		return tradeService.findById(id);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(int id) {
		tradeService.deleteById(id);
	}
	
	
	
}
