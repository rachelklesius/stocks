package com.citi.training.stocks.model;

import org.junit.Test;

public class TradeTests {

	@Test
	public void test_trade_constructor() {
		int tradeId = 1;
		String tradeStock = "AAPL";
		double tradePrice = 203.98;
		int tradeVolume = 40;
		
		Trade trade = 
				new Trade(tradeId, tradeStock, tradePrice, tradeVolume);
		
		assert(trade.getId() == tradeId);
		assert(trade.getStock().equals(tradeStock));
		assert(trade.getPrice() == tradePrice);
		assert(trade.getVolume() == tradeVolume);
	}
	
	@Test
	public void test_trade_setters() {
		int tradeId = 1;
		String tradeStock = "AAPL";
		double tradePrice = 203.98;
		int tradeVolume = 40;
		
		int newId = 4;
		String newStock = "GOOGL";
		double newPrice = 405.67;
		int newVolume = 30;
		
		Trade trade = 
				new Trade(tradeId, tradeStock, tradePrice, tradeVolume);
		
		trade.setId(newId);
		trade.setStock(newStock);
		trade.setPrice(newPrice);
		trade.setVolume(newVolume);
		
		assert(trade.getId() == newId);
		assert(trade.getStock().equals(newStock));
		assert(trade.getPrice() == newPrice);
		assert(trade.getVolume() == newVolume);
	}

}
